﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Telemetry.Service;
using Telemetry.Service.Entities;
using Telemetry.Service.Interfaces;
using Telemetry.WebAPI.Models;

namespace Telemetry.WebAPI.Controllers
{
    public class PlotsController : ApiController
    {

        private TempViewModel vm;
        private ITempRepo repo;

        public PlotsController()
        {
            this.repo = new TempRepo();
        }

        // GET api/plots
        public TempViewModel Get()
        {
            vm = new TempViewModel();
            IEnumerable<Temp> tempData = repo.GetData();
            vm.Data = tempData.Select(s => s.Calvin);
            vm.Span = tempData.Select(s => s.Time).Last() - tempData.Select(s => s.Time).First();
            vm.Start = tempData.First().Time;
            vm.Frequency = 1;

            return vm;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}


//foreach (var item in vm.Temperatures.Select(s => s.Calvin))
//{
//    string itemStr = Convert.ToString(item);
//    numStrs.Add(itemStr);
//}