﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Http;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Telemetry.WebAPI.Controllers;
using Telemetry.WebAPI.Models;

namespace Telemetry.WebUI.Tests.Controllers
{
    [TestClass]
    public class PlotsControllerTest
    {
        [TestMethod]
        public void APIGet()
        {
            // Arrange
            PlotsController controller = new PlotsController();

            // Act
            TempViewModel result = controller.Get() as TempViewModel;

            // Assert
            Assert.IsNotNull(result);
            
        }

        //[TestMethod]
        //public void About()
        //{
        //    // Arrange
        //    HomeController controller = new HomeController();

        //    // Act
        //    ViewResult result = controller.About() as ViewResult;

        //    // Assert
        //    Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        //}

        //[TestMethod]
        //public void Contact()
        //{
        //    // Arrange
        //    HomeController controller = new HomeController();

        //    // Act
        //    ViewResult result = controller.Contact() as ViewResult;

        //    // Assert
        //    Assert.IsNotNull(result);
        //}
    }
}
