﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telemetry.Service.Entities;

namespace Telemetry.Service.Interfaces
{
    public interface ITempRepo
    {

        List<Temp> GetData();

    }
}
